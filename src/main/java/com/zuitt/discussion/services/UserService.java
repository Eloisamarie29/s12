package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
    // create user
    void createUser(User user);
    // view all user
    Iterable<User> getAllUser();
    // delete user
    ResponseEntity deleteUser(Long id);
    //Update User
    ResponseEntity updateUser(Long id, User user);

}
