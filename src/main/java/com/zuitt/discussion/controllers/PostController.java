package com.zuitt.discussion.controllers;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
//  Enables Cross origin request via @CrossOrigin.
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;
    @PostMapping("/posts")
    //@RequestMapping(value="/posts",method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestBody Post post){
        postService.createPost(post);
        return new ResponseEntity<>("Post create successfully.", HttpStatus.CREATED);
    }

    @GetMapping("/posts")
    public ResponseEntity<Object> getPost(){
        return new ResponseEntity<>(postService.getPosts(),HttpStatus.OK);
    }
    @DeleteMapping("/posts/{postid}")
    public ResponseEntity<Object> deletePost(@PathVariable Long postid){
        return postService.deletePost(postid);
    }

}
